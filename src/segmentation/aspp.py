import torch.nn as nn
import torch.nn.functional as functional
import torch
from helper import init_weight


def backbone_mapping(backbone):
    mapping = {
        "drn": 512,
        "mobilenet": 320
    }
    in_channels = mapping.get(backbone)
    return in_channels if in_channels else 2048


def stride_mapping(output_stride):
    mapping = {
        16: [1, 6, 12, 18],
        8: [1, 12, 24, 36]
    }
    dilations = mapping.get(output_stride)
    if dilations is None:
        raise NotImplementedError
    return dilations


class _ASPP(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        padding,
        dilation,
        BatchNorm
    ):
        super(_ASPP, self).__init__()
        self.atrous_conv = nn.Conv2d(
            in_channels,
            out_channels,
            kernel_size=kernel_size,
            stride=1, padding=padding,
            dilation=dilation,
            bias=False
        )
        self.batchnorm = BatchNorm(out_channels)
        self.activate_func = nn.ReLU()
        init_weight(self.modules())

    def forward(self, input):
        input = self.atrous_conv(input)
        input = self.batchnorm(input)
        input = self.activate_func(input)
        return input


class ASPP(nn.Module):
    def __init__(self, backbone, output_stride, BatchNorm):
        super(ASPP, self).__init__()
        in_channels = backbone_mapping(backbone)
        dilations = stride_mapping(output_stride)

        self.aspp1 = _ASPP(
            in_channels=in_channels,
            out_channels=256,
            kernel_size=1,
            padding=0,
            dilation=dilations[0],
            BatchNorm=BatchNorm
        )
        self.aspp2 = _ASPP(
            in_channels=in_channels,
            out_channels=256,
            kernel_size=3,
            padding=dilations[1],
            dilation=dilations[1],
            BatchNorm=BatchNorm
        )
        self.aspp3 = _ASPP(
            in_channels=in_channels,
            out_channels=256,
            kernel_size=3,
            padding=dilations[2],
            dilation=dilations[2],
            BatchNorm=BatchNorm
        )
        self.aspp4 = _ASPP(
            in_channels=in_channels,
            out_channels=256,
            kernel_size=3,
            padding=dilations[3],
            dilation=dilations[3],
            BatchNorm=BatchNorm
        )
        self.global_avg_pool = nn.Sequential(
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Conv2d(
                in_channels, 256, 1, stride=1, bias=False
            ),
            BatchNorm(256),
            nn.ReLU())
        self.conv1 = nn.Conv2d(1280, 256, 1, bias=False)
        self.batchnorm = BatchNorm(256)
        self.activate_func = nn.ReLU()
        self.dropout = nn.Dropout(0.5)
        init_weight(self.modules())

    def forward(self, input):
        input1 = self.aspp1(input)
        input2 = self.aspp2(input)
        input3 = self.aspp3(input)
        input4 = self.aspp4(input)
        input5 = self.global_avg_pool(input)
        input5 = functional.interpolate(
            input5, size=input4.size()[
                2:], mode='bilinear', align_corners=True)
        input = torch.cat((input1, input2, input3, input4, input5), dim=1)
        input = self.conv1(input)
        input = self.batchnorm(input)
        input = self.activate_func(input)
        input = self.dropout(input)
        return input
