from torch.utils.data.dataset import Dataset
import torchvision.transforms as transforms
from glob import glob
from PIL import Image
import numpy as np


class ClothDataset(Dataset):
    def __init__(self, path):
        self.path = path
        self.normalize = transforms.Compose([
            transforms.ToTensor(),
            transforms.RandomHorizontalFlip(),
            transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
        ])
        self.imgs, self.labels = self.get_files(self.path)

    def get_files(self, path):
        imgs = glob(f"{path}/image/*")
        labels = glob(f"{path}/label/*")
        return imgs, labels

    def image_loader(self, data_path, label_path):
        data = Image.open(data_path)
        label = Image.open(label_path)
        return data, label

    def __getitem__(self, index):
        print(index)
        data_path, label_path = self.imgs[index], self.labels[index]
        img, label = self.image_loader(data_path, label_path)

        img = self.normalize(np.array(img))
        label = self.normalize(np.array(label))

        return img, label

    def __len__(self):
        return len(self.path)
