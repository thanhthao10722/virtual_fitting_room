import torch.nn as nn
from helper import init_weight
import torch.nn.functional as functional
import torch


def backbone_mapping(backbone):
    mapping = {
        "resnet": 256,
        "mobilenet": 24,
        "xception": 128
    }
    low_level_in_channels = mapping.get(backbone)
    if low_level_in_channels is None:
        raise NotImplementedError
    return low_level_in_channels


class Decoder(nn.Module):
    def __init__(self, num_classes, backbone, BatchNorm):
        super(Decoder, self).__init__()
        low_level_in_channels = backbone_mapping(backbone)
        self.conv1 = nn.Conv2d(low_level_in_channels, 48, 1, bias=False)
        self.batchnorm = BatchNorm(48)
        self.activate_func = nn.ReLU()
        self.last_conv = nn.Sequential(
            nn.Conv2d(304, 256, kernel_size=3,
                      stride=1, padding=1, bias=False),
            BatchNorm(256),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Conv2d(256, 256, kernel_size=3,
                      stride=1, padding=1, bias=False),
            BatchNorm(256),
            nn.ReLU(),
            nn.Dropout(0.1),
            nn.Conv2d(256, num_classes, kernel_size=1, stride=1))
        init_weight(self.modules())

    def forward(self, input, low_level_feat):
        low_level_feat = self.conv1(low_level_feat)
        low_level_feat = self.batchnorm(low_level_feat)
        low_level_feat = self.activate_func(low_level_feat)

        input = functional.interpolate(input, size=low_level_feat.size()[
            2:], mode='bilinear', align_corners=True)
        input = torch.cat((input, low_level_feat), dim=1)
        input = self.last_conv(input)
        return input
