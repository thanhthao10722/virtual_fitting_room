import torch.nn as nn
import torch.utils.model_zoo as model_zoo
import math
from cfg import RESET101_PATH


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(
        self,
        in_channels,
        out_channels,
        stride=1,
        dilation=1,
        downsample=None,
        BatchNorm=None
    ):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, out_channels,
                               kernel_size=1, bias=False)
        self.batchnorm1 = BatchNorm(out_channels)
        self.conv2 = nn.Conv2d(out_channels, out_channels, kernel_size=3,
                               stride=stride, dilation=dilation,
                               padding=dilation, bias=False)
        self.batchnorm2 = BatchNorm(out_channels)
        self.conv3 = nn.Conv2d(
            out_channels, out_channels * 4, kernel_size=1, bias=False)
        self.batchnorm3 = BatchNorm(out_channels * 4)
        self.activate_func = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride
        self.dilation = dilation

    def forward(self, input):
        residual = input
        output = self.conv1(input)
        output = self.batchnorm1(output)
        output = self.activate_func(output)

        output = self.conv2(output)
        output = self.batchnorm2(output)
        output = self.activate_func(output)

        output = self.conv3(output)
        output = self.batchnorm3(output)

        if self.downsample:
            residual = self.downsample(input)
        output += residual
        output = self.activate_func(output)
        return output


class Resnet(nn.Module):
    def __init__(
        self,
        block,
        layers,
        output_stride,
        BatchNorm,
        pretrained=True
    ):
        self.blocks = [1, 2, 4]
        self.in_channels = 64
        super(Resnet, self).__init__()
        if output_stride == 16:
            strides = [1, 2, 2, 1]
            dilations = [1, 1, 1, 2]
        elif output_stride == 8:
            strides = [1, 2, 1, 1]
            dilations = [1, 1, 2, 4]
        else:
            raise NotImplementedError

        self.conv1 = nn.Conv2d(
            3, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.batchnorm1 = BatchNorm(64)
        self.activate_func = nn.ReLU(inplace=True)
        self.max_pooling = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        self.layer1 = self._make_layer(
            block,
            64,
            layers[0],
            stride=strides[0],
            dilation=dilations[0],
            BatchNorm=BatchNorm
        )
        self.layer2 = self._make_layer(
            block,
            128,
            layers[1],
            stride=strides[1],
            dilation=dilations[1],
            BatchNorm=BatchNorm
        )
        self.layer3 = self._make_layer(
            block,
            256,
            layers[2],
            stride=strides[2],
            dilation=dilations[2],
            BatchNorm=BatchNorm
        )
        self.layer4 = self._make_MG_unit(
            block,
            512,
            blocks=self.blocks,
            stride=strides[3],
            dilation=dilations[3],
            BatchNorm=BatchNorm
        )
        self._init_weight()
        if pretrained:
            self._load_pretrained_model()

    def _make_layer(
        self,
        block,
        out_channels,
        blocks,
        stride=1,
        dilation=1,
        BatchNorm=None
    ):
        downsample = None
        if stride != 1 or self.in_channels != out_channels * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.in_channels, out_channels * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                BatchNorm(out_channels * block.expansion),
            )

        layers = []
        layers.append(block(self.in_channels, out_channels, stride,
                            dilation, downsample, BatchNorm))
        self.in_channels = out_channels * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.in_channels, out_channels,
                                dilation=dilation, BatchNorm=BatchNorm))

        return nn.Sequential(*layers)

    def _make_MG_unit(
        self,
        block,
        out_channels,
        blocks,
        stride=1,
        dilation=1,
        BatchNorm=None
    ):
        downsample = None
        if stride != 1 or self.in_channels != out_channels * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.in_channels, out_channels * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                BatchNorm(out_channels * block.expansion),
            )

        layers = []
        layers.append(block(
            self.in_channels, out_channels,
            stride, dilation=blocks[0] * dilation,
            downsample=downsample,
            BatchNorm=BatchNorm))
        self.in_channels = out_channels * block.expansion
        for i in range(1, len(blocks)):
            layers.append(block(
                self.in_channels, out_channels, stride=1,
                dilation=blocks[i] * dilation, BatchNorm=BatchNorm))

        return nn.Sequential(*layers)

    def _init_weight(self):
        for module in self.modules():
            if isinstance(module, nn.Conv2d):
                temp = module.kernel_size[0] * \
                    module.kernel_size[1] * module.out_channels
                module.weight.data.normal_(0, math.sqrt(2. / temp))
            elif isinstance(module, nn.BatchNorm2d):
                module.weight.data.fill_(1)
                module.bias.data.zero_()

    def forward(self, input):
        output = self.conv1(input)
        output = self.batchnorm1(output)
        output = self.activate_func(output)
        output = self.max_pooling(output)

        output = self.layer1(output)
        low_level_feat = output
        output = self.layer2(output)
        output = self.layer3(output)
        output = self.layer4(output)

        return output, low_level_feat

    def _load_pretrained_model(self):
        pretrained_model = model_zoo.load_url(RESET101_PATH)
        model_dict = {}
        state_dict = self.state_dict()
        for k, v in pretrained_model.items():
            if k in state_dict:
                model_dict[k] = v
        state_dict.update(model_dict)
        self.load_state_dict(state_dict)


def ResNet101(output_stride, BatchNorm):
    return Resnet(Bottleneck, [3, 4, 23, 3], output_stride, BatchNorm)


if __name__ == "__main__":
    import torch
    model = ResNet101(BatchNorm=nn.BatchNorm2d, output_stride=8)
    input = torch.rand(1, 3, 512, 512)
    output, low_level_feat = model(input)
    print(output.size())
    print(low_level_feat.size())
