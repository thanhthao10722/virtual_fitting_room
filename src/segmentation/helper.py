import torch.nn as nn
import torch


def init_weight(modules):
    for module in modules:
        if isinstance(module, nn.Conv2d):
            torch.nn.init.kaiming_normal_(module.weight)
        elif isinstance(module, nn.BatchNorm2d):
            module.weight.data.fill_(1)
            module.bias.data.zero_()
