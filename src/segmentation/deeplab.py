from resnet101 import ResNet101
from aspp import ASPP
from decoder import Decoder
import torch.nn as nn
import torch.nn.functional as functional
import torch
from torchsummary import summary


class DeepLab(nn.Module):
    def __init__(
        self,
        BatchNorm,
        backbone="resnet",
        output_stride=16,
        num_classes=2,
        freeze_bn=False
    ):
        super(DeepLab, self).__init__()
        if backbone == "drn":
            output_stride = 8
        self.backbone = ResNet101(output_stride, BatchNorm)
        self.aspp = ASPP(backbone, output_stride, BatchNorm)
        self.decoder = Decoder(num_classes, backbone, BatchNorm)

        if freeze_bn:
            for module in self.modules():
                if isinstance(module, nn.BatchNorm2d):
                    module.eval()

    def forward(self, input):
        x, low_level_feat = self.backbone(input)
        x = self.aspp(x)
        x = self.decoder(x, low_level_feat)
        x = functional.interpolate(x, size=input.size()[
            2:], mode='bilinear', align_corners=True)

        n, c, h, w = x.size()
        output = torch.rand(n, h, c * w)
        for i in range(n):
            output[i] = torch.cat((x[i][0], x[i][1]), 1)
        return output


if __name__ == "__main__":
    model = DeepLab(nn.BatchNorm2d)
    summary(model, (3, 224, 224))
