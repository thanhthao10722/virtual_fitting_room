import torch
import torch.nn as nn
import copy
from deeplab import DeepLab
from torch import optim
from torch.optim import lr_scheduler
import matplotlib
import numpy as np
from img_to_tensor import ClothDataset
matplotlib.use('agg')

import matplotlib.pyplot as plt


def train_model(
    model,
    device,
    criterion,
    optimizer,
    scheduler,
    num_epochs,
    data_path="dataset/data_test",
    tracking_file_path="output/tracking_train.txt"
):
    file = open(tracking_file_path)
    data_train = ClothDataset(f"{data_path}/train")
    data_val = ClothDataset(f"{data_path}/validation")
    dataloader_train = torch.utils.data.DataLoader(
        data_train, batch_size=4,
        shuffle=True)
    dataloader_val = torch.utils.data.DataLoader(
        data_val, batch_size=4,
        shuffle=True)
    for inputs, lb in dataloader_train:
        print(inputs)
        print(lb)
    dataloaders = {
        "train": dataloader_train,
        "val": dataloader_val
    }
    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0
    train_loss = []
    val_loss = []
    train_acc = []
    val_acc = []
    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        for phase in ["train", "val"]:
            if phase == "train":
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for index, (inputs, labels) in dataloaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == "train"):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == "train":
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)
            if phase == "train":
                scheduler.step()
                epoch_loss = running_loss / len(X_train)
                epoch_acc = running_corrects.double() / len(X_train)
                train_acc.append(epoch_acc)
                train_loss.append(epoch_loss)
                file.writelines(
                    f"Epoch {epoch}: Loss=>{epoch_loss}, Acc=>{epoch_acc}\n"
                )

            else:
                epoch_loss = running_loss / len(X_val)
                epoch_acc = running_corrects.double() / len(X_val)
                val_acc.append(epoch_acc)
                val_loss.append(epoch_loss)

            print("{} Loss: {:.4f} Acc: {:.4f}".format(
                phase, epoch_loss, epoch_acc))

            # deep copy the model
            if phase == "val" and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())

    # load best model weights
    model.load_state_dict(best_model_wts)
    train_info = {
        "train_acc": train_acc,
        "train_loss": train_loss,
        "val_acc": val_acc,
        "val_loss": val_loss
    }
    return model, train_info


def draw_trained_model_history(epochs, train_info):
    # fig = plt.figure()
    plt.plot(np.arange(0, epochs), train_info["train_loss"], label="loss")
    plt.plot(np.arange(0, epochs),
             train_info["val_loss"], label="validation loss")
    plt.plot(np.arange(0, epochs), train_info["train_acc"], label="accuracy")
    plt.plot(np.arange(0, epochs),
             train_info["val_acc"], label="validation accuracy")
    plt.title("Accuracy and Loss")
    plt.xlabel("Epoch")
    plt.ylabel("Loss|Accuracy")
    plt.legend()
    plt.savefig(f"output/model_history.png")
    plt.show()


if __name__ == '__main__':
    output_path = "output/clothes_segmentation_model.pth"
    model_ft = DeepLab(nn.BatchNorm2d)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model_ft = model_ft.to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer_ft = optim.SGD(model_ft.parameters(), lr=0.001, momentum=0.9)
    exp_lr_scheduler = lr_scheduler.StepLR(
        optimizer_ft, step_size=50, gamma=0.1
    )
    num_epochs = 2
    model, train_info = train_model(
        model=model_ft,
        device=device,
        criterion=criterion,
        optimizer=optimizer_ft,
        scheduler=exp_lr_scheduler,
        num_epochs=num_epochs
    )
    torch.save(model.model.state_dict(), output_path)
    draw_trained_model_history(num_epochs, train_info)
