from preprocess_dataset import ImageProcessing

if __name__ == '__main__':
    img_processing = ImageProcessing((224, 224))
    data_dirs = {
        "label_image/train": "data/train/label",
        "dataset/train/image": "data/train/image",
    }
    for key, value in data_dirs.items():
        img_processing.resize_image(
            key,
            value
        )
