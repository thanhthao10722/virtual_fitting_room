from .preprocess_dataset import ImageProcessing


def __sif__():
    img_processing = ImageProcessing((224, 224))
    return img_processing


img_processing = __sif__()

__all__ = (
    "img_processing", "preprocess_dataset"
)
