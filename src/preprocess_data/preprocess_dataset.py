import os
import numpy as np
import cv2
from PIL import Image
import json
from cfg import SHIRT_CATEGORY


class ImageProcessing:
    def __init__(self, image_size):
        self.image_size = image_size

    def image_to_tensor(self, path):
        X = []
        Y = []
        for file_path in os.listdir(f"{path}/image"):
            img = Image.open(f"{path}/image/{file_path}")
            basename = os.path.basename(file_path)
            img_label = Image.open(f"{path}/label/{basename}")
            X.append(np.array(img))
            Y.append(np.array(img_label))
        return X, Y

    def resize_image(self, path, output_dir):
        for filename in os.listdir(path):
            if filename.endswith('.jpg') or filename.endswith('.png'):
                img = Image.open(f"{path}/{filename}")
                weight, height = img.size
                img = img.resize(self.image_size)
                save_image = Image.fromarray(np.array(img))
                save_image.save(f"{output_dir}/{filename}")

    def draw_image_with_polygon(
        self, file_path, json_path, output_dir, tracking_file
    ):
        img = cv2.imread(file_path)
        basename = os.path.basename(file_path)
        f = open(json_path)
        data = json.load(f)
        redundancy_images = open(
            tracking_file, "a"
        )
        mask = np.zeros(
            (img.shape[0], img.shape[1], 3),
            dtype=np.uint8
        )

        polygon_data = {}
        for key in data.keys():
            if key.startswith("item"):
                polygon_data[data[key]["category_id"]
                             ] = data[key]["segmentation"]
        count = 0
        for key, value in polygon_data.items():
            if key in SHIRT_CATEGORY:
                count += 1
                for segmentation in value:
                    draw_data = []
                    for index in range(0, len(segmentation), 2):
                        draw_data.append(
                            [segmentation[index], segmentation[index + 1]]
                        )
                    pts = np.array(draw_data, np.int32)
                    self.draw_contours(mask, pts, (255, 255, 255))
                cv2.imwrite(f"{output_dir}/{basename}", mask)
        if count == 0:
            redundancy_images.writelines(f"{file_path}\n")
            os.remove(file_path)
        f.close()

    def draw_contours(self, mask, pts, color):
        cv2.drawContours(mask, [pts], -1, color, -1)

    def preprocess_image(self, input_dir, output_dir, tracking_file):
        for file_path in os.listdir(f"{input_dir}/image"):
            basename = os.path.basename(file_path)
            file_name = os.path.splitext(basename)[0]
            json_file_path = f"{input_dir}/annos/{file_name}.json"
            self.draw_image_with_polygon(
                f"{input_dir}/image/{file_path}",
                json_file_path,
                output_dir,
                tracking_file
            )


if __name__ == '__main__':
    data_dir = {
        "train": "dataset/train/image",
        "val": "dataset/validation/image"
    }
    obj = ImageProcessing((224, 224))
    obj.preprocess_image(
        "dataset/train",
        "label_image/train",
        "output/redundancy_images.txt"
    )
