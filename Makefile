clean-py-binary:
	find ./src ./cfg -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete

install:
	pip install -r requirements.txt

train:
	PYTHONPATH=./src/segmentation:./src/preprocess_dataset:./src python -m train_model

process-data:
	PYTHONPATH=./src/preprocess_dataset python -m preprocess_dataset
	PYTHONPATH=./src/preprocess_dataset python -m resize_image

run:
	PYTHONPATH=./src/segmentation python -m deeplab
