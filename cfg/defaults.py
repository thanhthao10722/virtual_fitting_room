RESET101_PATH = "https://download.pytorch.org/models/resnet101-5d3b4d8f.pth"
CATEGORY_COLOR = {
    1: (255, 255, 255),
    2: (141, 153, 150),
    3: (7, 34, 171),
    4: (235, 108, 184),
    5: (150, 227, 77)
}
SHIRT_CATEGORY = [1, 2, 3, 4, 5, 6, 10, 11, 12, 13]
